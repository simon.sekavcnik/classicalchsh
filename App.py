from XOR.classical import Game
import matplotlib.pyplot as plt
import numpy as np
from operator import itemgetter

resolution = 1001


supremums = []

for i in range(2,13):
    print("Analyzin game with " + str(i) + " Players")
    game = Game(number_of_players=i, resolution=resolution)
    supremums.append(game.GetSupremum())




plt.title("Supremums of N-Players")
plt.xlabel("Input question probability p_1")
plt.ylabel("Win rate")

for i in range(len(supremums)):
    plt.plot(np.linspace(0,1,resolution), supremums[i], label=str(i+2)+" players")

plt.legend()
plt.savefig("supremums.png")
