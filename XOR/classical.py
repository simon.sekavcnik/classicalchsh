import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from pathlib import Path


class Player:
    # Define 4 deterministic strategies
    def get_answer(self, strategy, question):
        question = bool(question)
        if strategy == 0:
            return False
        if strategy == 1:
            return True
        if strategy == 2:
            return question
        if strategy == 3:
            return (not question)


class Game:
    #Class Constructor
    def __init__(self, number_of_players, resolution=None):
        self.number_of_players = number_of_players
        if resolution == None:
            self.resolution = 101
        else:
            self.resolution = resolution
        self.players = []
        for i in range(number_of_players):
            self.players.append(Player())
        #Every player can be asked 0=false or 1=true
        single_question = [False, True]

        #All possible combinations of question strings
        self.questions = list(product(single_question, repeat=number_of_players))
        #All possible combinations of player strategies
        self.strategies = list(product(range(4), repeat=number_of_players))
        
        self.variation = []
        self.gameResults= []
        self.gameResultsNoStrategy = []

        #Ensure that dir exists
        Path("graphs").mkdir(parents=True, exist_ok=True)

    def Play(self):
        for strategy in self.strategies:
            #[[questions],[answers],outcome]
            result = []
            for questions in self.questions:
                answers = []
                for i in range(len(self.players)):
                    answers.append(self.players[i].get_answer(strategy[i],questions[i]))

                outcome = self.WinCondition(answers, questions)
                result.append([list(questions), answers, outcome])
            self.gameResults.append(result)
        
    def _PlayWithStrategy(self, strategy):
        result = []
        for question in self.questions:
            answers = []
            for i in range(len(self.players)):
                answers.append(self.players[i].get_answer(strategy[i],question[i]))

            outcome = self.WinCondition(answers, question)
            result.append([list(question), answers, outcome])
        self.gameResultsNoStrategy.append(result)
        return result

    def GetSupremum(self):
        strategies = self._getOptimumStrategies()
        results = []
        variation = []
        supremum = [0]*self.resolution
        for strategy in strategies:
            results.append(self._PlayWithStrategy(strategy))

        variation = self._AnalyzeWithoutStrategy(results)
        #print(variation)

        for v in variation:
            for i in range(self.resolution):
                if (v[i]>supremum[i]):
                    supremum[i] = v[i]

        return supremum

            


    def _getOptimumStrategies(self):
        optimumStrategies = []
        #Strategy 0
        s0 = [0]*self.number_of_players

        s1 = [2]*self.number_of_players
        if self.number_of_players%2==0:
            s1[-1] = 3

        optimumStrategies.append(tuple(s0))
        optimumStrategies.append(tuple(s1))
        return optimumStrategies


        
    def _AnalyzeWithoutStrategy(self, result=None):
        if result == None:
            result = self.gameResultsNoStrategy
        variation = []
        for res in result:
            win_array = []
            probability_spectrum = np.linspace(0,1,self.resolution)
            for p_1 in probability_spectrum:
                q_prob = self.GetQuestionProbabilityArray(p_1)
                win_rate = 0 
                for i in range(len(q_prob)):
                    if(res[i][-1]):
                        win_rate = win_rate + q_prob[i]
                win_array.append(win_rate)

            variation.append(win_array)
        return variation

    def WinCondition(self, answers, questions):
        x = 1
        for question in questions:
            x = x*question
        x = bool(x)
        y = None
        for i in range(len(answers)-1):
            if(y == None):
                y = answers[i]^answers[i+1]
            else:
                y = y^answers[i+1]
        if( x == y ):
            return True
        return False

    def AnalyzeOneStrategy(self,global_strategy):
        probability_spectrum = np.linspace(0,1,self.resolution)
        res = self.gameResults[global_strategy]
        win_array = []
        
        for p_1 in probability_spectrum:
            q_prob = self.GetQuestionProbabilityArray(p_1)
            win_rate = 0 
            for i in range(len(q_prob)):
                if(res[i][-1]):
                    win_rate = win_rate + q_prob[i]
            win_array.append(win_rate)

        self.variation.append(win_array)

    def GetQuestionProbabilityArray(self, p_1):
        p_0 = 1-p_1
        probability_combinations = list(product([p_0,p_1], repeat=self.number_of_players))
        string_probability = []
        for combination in probability_combinations:
            x = 1
            for a in combination:
                x = x*a
            string_probability.append(x)
        return string_probability

    def AnalyzeAllStrategies(self):
        for global_strategy in range(len(self.strategies)):
            self.AnalyzeOneStrategy(global_strategy)


    def PlotAll(self, fileName=None):
        #plt.figure(1)
        plt.title("Classical win probability of " + str(self.number_of_players) + " players"+
                "\n All deterministic strategies")
        plt.xlabel("Input question probability p_1")
        plt.ylabel("Win rate")
        for global_strategy in range(len(self.strategies)):
            plt.plot(np.linspace(0,1,self.resolution),self.variation[global_strategy])

        if(fileName==None):
            plt.show()
        else:
            fileName = "graphs/"+fileName
            plt.savefig(fileName)
        plt.clf()

    def PlotSupremum(self, fileName=None):
        supremum = np.zeros(self.resolution)
        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
        #plt.figure(2)
        plt.title("Classical win probability of " + str(self.number_of_players) + " players" +
                "\n Sumpremum of all deterministic strategies")
        plt.xlabel("Input question probability p_1")
        plt.ylabel("Win rate")
        plt.plot(np.linspace(0,1,self.resolution), supremum)
        if(fileName==None):
            plt.show()
        else:
            fileName = "graphs/"+fileName
            plt.savefig(fileName)
        plt.clf()
    '''
    def GetSupremum(self):
        supremum = np.zeros(self.resolution)
        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
        return supremum
    '''
    def GetSupremumWithStrategies(self):
        supremum = np.zeros(self.resolution)
        strategies = [None]*self.resolution
        for y in range(len(self.variation)):
            for x in range(len(supremum)):
                if self.variation[y][x] > supremum[x]:
                    supremum[x] = self.variation[y][x]
                    strategies[x] = self.strategies[y]
        optimum_strategies = []
        optimum_strategies_range = []

        for i in range(len(strategies)):
            if i == 0:
                optimum_strategies.append(strategies[0])
                optimum_strategies_range.append([0])
            else:
                if strategies[i] != optimum_strategies[-1]:
                    optimum_strategies.append(strategies[i])
                    optimum_strategies_range[-1].append((i-1)/self.resolution)
                    optimum_strategies_range.append([i/self.resolution])

        optimum_strategies_range[-1].append(1)
        print(optimum_strategies_range)

        return supremum, optimum_strategies


    def PlotAllSeparate(self):
        dirName = str(self.number_of_players) + "_separate"
        Path("graphs/"+dirName).mkdir(parents=True, exist_ok=True)
        for i in range(len(self.variation)):
            plt.title("Classical win probability: Separate Strategies of " + str(self.number_of_players)
                    + " players \n" + "Strategy: " + str(self.strategies[i]) )
            plt.xlabel("Input question probability p_1")
            plt.ylabel("Win rate")
            plt.plot(np.linspace(0,1,self.resolution),self.variation[i])

            plotName = str(self.number_of_players)+"-"+str(self.strategies[i]).replace(" ","")+".png"

            plt.savefig("graphs/"+dirName + "/" + plotName)
            plt.clf()

